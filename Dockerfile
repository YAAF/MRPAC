FROM python:3.10-slim

WORKDIR /MRPAC

COPY . .

RUN pip install --upgrade build
RUN python -m build
RUN pip install .