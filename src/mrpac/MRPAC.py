"""The main module for MRPAC."""

import logging
import os
import sqlite3
import sys
import traceback
import bcrypt
from PySide6 import QtCore as qtc
from PySide6 import QtWidgets as qtw
from PySide6 import QtGui as qtg

from .login import LoginWindow
from ._globals import Globals

# Initialize the Logger files
if not os.path.exists(Globals.LOGS_DIRECTORY):
    os.makedirs(Globals.LOGS_DIRECTORY)
pynet_logger = logging.getLogger("network")
pynet_logger.setLevel(logging.DEBUG)
mrpac_logger = logging.getLogger("mrpac")
mrpac_logger.setLevel(logging.DEBUG)
autocontour_logger = logging.getLogger("autocontour")
autocontour_logger.setLevel(logging.DEBUG)
database_logger = logging.getLogger("database")
database_logger.setLevel(logging.DEBUG)
file_handler_pynet = logging.FileHandler(os.path.join(Globals.LOGS_DIRECTORY, "network.log"))
file_handler_pynet.setFormatter(Globals.LOG_FORMATTER)
file_handler_mrpac = logging.FileHandler(os.path.join(Globals.LOGS_DIRECTORY, "mrpac.log"))
file_handler_mrpac.setFormatter(Globals.LOG_FORMATTER)
file_handler_autocontour = logging.FileHandler(
    os.path.join(Globals.LOGS_DIRECTORY, "autocontour.log")
)
file_handler_autocontour.setFormatter(Globals.LOG_FORMATTER)
file_handler_database = logging.FileHandler(os.path.join(Globals.LOGS_DIRECTORY, "database.log"))
file_handler_database.setFormatter(Globals.LOG_FORMATTER)
pynet_logger.addHandler(file_handler_pynet)
mrpac_logger.addHandler(file_handler_mrpac)
autocontour_logger.addHandler(file_handler_autocontour)
database_logger.addHandler(file_handler_database)


def databaseSetup() -> None:
    """Sets up the database and creates neccesary tables if they are not created already."""

    createHostTable = 'CREATE TABLE "host" \
                        ("id" INTEGER, \
                         "hostAET" TEXT, \
                         "hostIP" TEXT, \
                         "hostPort" TEXT, \
                         PRIMARY KEY("id" AUTOINCREMENT))'
    createServersTable = 'CREATE TABLE "servers" \
                          ("id" INTEGER, \
                           "serverAET" TEXT, \
                           "serverIP" TEXT, \
                           "serverPort" TEXT, \
                           PRIMARY KEY("id" AUTOINCREMENT))'
    createUsersTable = 'CREATE TABLE "users" \
                        ("id" INTEGER, \
                         "userName" TEXT, \
                         "passPhrase" TEXT, \
                         "userType" TEXT, \
                         PRIMARY KEY("id" AUTOINCREMENT))'
    createHistoryTable = 'CREATE TABLE "history" \
                          ("id" INTEGER, \
                           "patientName" TEXT, \
                           "patientID" TEXT, \
                           "time" TEXT, \
                           "status" TEXT, \
                           "errors" TEXT, \
                           PRIMARY KEY("id" AUTOINCREMENT))'

    try:
        Globals.db_cur.execute(createHostTable)
        Globals.db_con.commit()
    except sqlite3.OperationalError:
        pass

    try:
        Globals.db_cur.execute(createServersTable)
        Globals.db_con.commit()
    except sqlite3.OperationalError:
        pass

    try:
        Globals.db_cur.execute(createUsersTable)
        Globals.db_con.commit()
        query = "INSERT INTO users (userName, passPhrase, userType) VALUES (?, ?, ?)"
        passPhrase = bcrypt.hashpw(b"mrpac", bcrypt.gensalt()).decode()
        Globals.db_cur.execute(query, ("Admin", passPhrase, "Administrator"))
        Globals.db_con.commit()

    except sqlite3.OperationalError:
        pass

    try:
        Globals.db_cur.execute(createHistoryTable)
        Globals.db_con.commit()
    except sqlite3.OperationalError:
        pass


def logger_db_setup(db_path):
    # SQL statements to create the tables, now including StackTrace column
    table_creations = {
        "mrpac_log": """
            CREATE TABLE IF NOT EXISTS mrpac_log (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                Timestamp DATETIME,
                LogLevel TEXT,
                Message TEXT,
                LineNumber INTEGER,
                StackTrace TEXT
            );
        """,
        "autocontour_log": """
            CREATE TABLE IF NOT EXISTS autocontour_log (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                Timestamp DATETIME,
                LogLevel TEXT,
                Message TEXT,
                LineNumber INTEGER,
                StackTrace TEXT
            );
        """,
        "database_log": """
            CREATE TABLE IF NOT EXISTS database_log (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                Timestamp DATETIME,
                LogLevel TEXT,
                Message TEXT,
                LineNumber INTEGER,
                StackTrace TEXT
            );
        """,
        "network_log": """
            CREATE TABLE IF NOT EXISTS network_log (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                Timestamp DATETIME,
                LogLevel TEXT,
                Message TEXT,
                LineNumber INTEGER,
                StackTrace TEXT
            );
        """,
    }

    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()

    # Execute each table creation SQL statement
    for table_sql in table_creations.values():
        cursor.execute(table_sql)

    # Commit changes and close the connection
    conn.commit()
    conn.close()


def start():
    # Initiate the logger database
    logger_db_setup(Globals.log_db_path)

    # Initiate the database connection
    if not os.path.exists(Globals.RESOURCES_DIRECTORY):
        os.makedirs(Globals.RESOURCES_DIRECTORY)
    try:
        db_path = os.path.join(Globals.RESOURCES_DIRECTORY, "entries.db")
        Globals.db_con = sqlite3.connect(db_path, check_same_thread=False)
        Globals.db_cur = Globals.db_con.cursor()

    except Exception as e:
        database_logger.error(e)
        database_logger.debug(e, exc_info=True)
        stack_trace = traceback.format_exc()
        Globals.log_to_db("database", "ERROR", str(e), None)
        Globals.log_to_db("database", "DEBUG", str(e), stack_trace)

    databaseSetup()

    # start the application
    mrpac_logger.info("Starting MRPAC ...")
    Globals.log_to_db("mrpac", "INFO", "Starting MRPAC ...", None)
    APP = qtw.QApplication(sys.argv)
    login = LoginWindow()  # noqa

    try:
        sys.exit(APP.exec())
    except Exception as e:
        stack_trace = traceback.format_exc()
        Globals.log_to_db("mrpac", "ERROR", str(e), None)
        Globals.log_to_db("mrpac", "DEBUG", str(e), None)

        print(e)


if __name__ == "__main__":

    start()
