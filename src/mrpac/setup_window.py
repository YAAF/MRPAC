import logging

from PySide6 import QtCore as qtc
from PySide6 import QtWidgets as qtw
from PySide6 import QtGui as qtg

from ._globals import Globals
from .DICOM_Networking import (
    StorageSCP,
    pingTest,
    validEntry,
    verifyEcho,
    handle_open,
    handle_store,
    handle_close,
)
from .ui_setup import Ui_MainWindow
from .config_window import ConfigWindow
from .verify_window import VerifyWindow

mrpac_logger = logging.getLogger("mrpac")
database_logger = logging.getLogger("database")
pynet_logger = logging.getLogger("network")


class SetupWindow(qtw.QMainWindow, Ui_MainWindow):
    """The setup window configuration."""

    def __init__(self):
        """Initialize the setup window when login is successful."""

        super().__init__()
        self.setupUi(self)
        self.setFixedHeight(400)
        self.setFixedWidth(600)
        self.show()

        self.startSCPButton.clicked.connect(self.startSCPFunc)
        self.stopSCPButton.clicked.connect(self.stopSCPFunc)
        self.saveButton.clicked.connect(self.saveFunc)
        self.configButton.clicked.connect(self.configFunc)
        self.verifyConButton.clicked.connect(self.verifyConFunc)

        self.startUp()

    def startUp(self):
        """Populates the SCP details if saved before."""
        query = "SELECT * FROM host"
        Globals.db_cur.execute(query)
        host_info = Globals.db_cur.fetchone()
        if host_info:
            self.scpAETEntry.setText(host_info[1])
            self.scpPortEntry.setText(host_info[3])

    def startSCPFunc(self):
        """Start the DICOM SCP server to accept C-Move requests."""

        Globals.scpAET = self.scpAETEntry.text()
        Globals.scpPort = self.scpPortEntry.text()
        if Globals.scpAET == "" or Globals.scpPort == "":
            self.invalidLabel.setText("Fields cannot be empty")
            mrpac_logger.error("SETUP--SCP Details: The AE title and port fields cannot be empty.")
        elif not validEntry(Globals.scpAET, "AET"):
            self.invalidLabel.setText("Invalid AE Title")
            mrpac_logger.error("SETUP--SCP Details: Invalid AE Title.")
        elif not validEntry(Globals.scpPort, "Port"):
            self.invalidLabel.setText("Invalid port number")
            mrpac_logger.error("SETUP--SCP Details: Invalid port number.")
        else:
            try:
                Globals.scpPort = int(Globals.scpPort)
                self.new_scp = StorageSCP(Globals.scpAET, Globals.HOSTIP, Globals.scpPort)
                self.new_scp.set_handlers(
                    handle_open=handle_open,
                    handle_close=handle_close,
                    handle_store=handle_store,
                )
                self.new_scp.start()
                self.invalidLabel.setText("")
                self.statusLabel.setText("Active")
                self.statusLabel.setStyleSheet(
                    "color: rgb(100, 255, 100);font: 10pt MS Shell Dlg 2"
                )
                self.cstoreAETLabel.setText("DICOM Store AET:")
                self.cstoreAETEntry.setText(Globals.scpAET)
                self.cstorePortLabel.setText("Port:")
                self.cstorePortEntry.setText(str(Globals.scpPort))
            except ValueError:
                self.invalidLabel.setText("Invalid port number")
                mrpac_logger.e("SETUP--SCP Details: Invalid port number.")
            except Exception as e:
                mrpac_logger.error(e)
                mrpac_logger.debug(e, exc_info=True)

    def stopSCPFunc(self):
        """Stop the DICOM SCP server if it was running."""

        try:
            self.new_scp.stop()
            self.statusLabel.setText("Inactive")
            self.statusLabel.setStyleSheet("color: rgb(255, 255, 255);font: 10pt MS Shell Dlg 2")
            self.cstoreAETLabel.setText("")
            self.cstoreAETEntry.setText("")
            self.cstorePortLabel.setText("")
            self.cstorePortEntry.setText("")

        except AttributeError:
            self.invalidLabel.setText("SCP is inactive")
            mrpac_logger.error("SETUP--SCP Status: SCP is inactive.")
        except ValueError:
            self.invalidLabel.setText("SCP is inactive")
            mrpac_logger.error("SETUP--SCP Status: SCP is inactive")

    def saveFunc(self):
        """Save the SCP Details."""

        scpAET = self.scpAETEntry.text()
        scpPort = self.scpPortEntry.text()
        if scpAET == "" or scpPort == "":
            self.invalidLabel.setText("Fields cannot be empty")
            mrpac_logger.error("SETUP--SCP Details: AE title and port fields cannot be empty.")
        elif not validEntry(scpAET, "AET"):
            self.invalidLabel.setText("Invalid AE Title")
            mrpac_logger.error("SETUP--SCP Details: Invalid AE Title.")
        elif not validEntry(scpPort, "Port"):
            self.invalidLabel.setText("Invalid port number")
            mrpac_logger.error("SETUP--SCP Detials: Invalid port number.")
        else:
            try:
                scpPort = int(scpPort)
                try:
                    query = "SELECT * FROM host"
                    savedEntry = Globals.db_cur.execute(query).fetchone()
                    if savedEntry is None:
                        query = "INSERT INTO host (hostAET, hostIP, hostPort) VALUES (?, ?, ?)"
                        Globals.db_cur.execute(query, (scpAET, Globals.HOSTIP, scpPort))
                        Globals.db_con.commit()
                    else:
                        query = (
                            "UPDATE host set hostAET = ?, hostIP = ?, hostPort = ? WHERE id = 1"
                        )
                        Globals.db_cur.execute(query, (scpAET, Globals.HOSTIP, scpPort))
                except Exception as e:
                    database_logger.debug(e)
                    database_logger.debug(e, exc_info=True)

            except ValueError:
                self.invalidLabel.setText("Invalid port number")
                mrpac_logger.error("SETUP--SCP Details: Invalid port number.")

    def configFunc(self):
        """Start the configuration window."""
        Globals.config_screen = ConfigWindow()

    def verifyConFunc(self):
        """Verify the DICOM connection using ping and C-Echo."""

        serverAET = self.currentServerAET.text()
        serverIP = self.currentServerIP.text()
        serverPort = self.currentServerPort.text()
        pingResult = None
        echoResult = None

        if serverAET != "" and serverIP != "" and serverPort != "":
            try:
                pingResult = pingTest(serverIP)
            except Exception as e:
                pingResult = "Failed"
                pynet_logger.debug(e)
                pynet_logger.debug(e, exc_info=True)

            try:
                echoResult = verifyEcho(Globals.scpAET, serverAET, serverIP, int(serverPort))
            except Exception as e:
                pynet_logger.debug(e)
                pynet_logger.debug(e, exc_info=True)
                echoResult = "Failed"

        self.verify = VerifyWindow(pingResult, echoResult)

    def serverSelected(self):
        """Select a DICOM location to send RTstruct file."""

        serverAET = Globals.selected_server[1]
        serverIP = Globals.selected_server[2]
        serverPort = Globals.selected_server[3]
        try:
            self.currentServerAET.setText(serverAET)
            self.currentServerIP.setText(serverIP)
            self.currentServerPort.setText(serverPort)
        except Exception as e:
            mrpac_logger.error(e)
            mrpac_logger.debug(e, exc_info=True)

    def closeEvent(self, event):
        """Log when window closes.

        Args:
            event (_type_): _description_
        """

        mrpac_logger.info("Closing MRPAC.")
        event.accept()
