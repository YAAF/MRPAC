import logging


from PySide6 import QtCore as qtc
from PySide6 import QtWidgets as qtw
from PySide6 import QtGui as qtg

from ._globals import Globals
from .ui_config import Ui_ConfigWindow
from .add_window import AddWindow
from .edit_window import EditWindow

mrpac_logger = logging.getLogger("mrpac")
database_logger = logging.getLogger("database")
pynet_logger = logging.getLogger("network")


class ConfigWindow(qtw.QWidget, Ui_ConfigWindow):
    """The configuration window."""

    def __init__(self):
        """Initialize the configuration window."""
        super().__init__()

        self.setupUi(self)
        self.setFixedHeight(220)
        self.setFixedWidth(400)
        self.addButton.clicked.connect(self.addFunc)
        self.removeButton.clicked.connect(self.removeFunc)
        self.editButton.clicked.connect(self.editFunc)
        self.selectButton.clicked.connect(self.selectFunc)

        self.startUp()

        self.listWidget.itemClicked.connect(self.itemClicked_event)
        self.listWidget.itemActivated.connect(self.itemActivated_event)
        self.selectedItem = None
        self.show()

    def startUp(self):
        """Populate the configuration screen with saved DICOM locations."""

        self.listWidget.clear()
        query = "SELECT * FROM servers"
        self.serversCode = {}
        try:
            all_servers = Globals.db_cur.execute(query).fetchall()
            indx = 1
            for server in all_servers:
                self.listWidget.addItem(f"{indx} {server[1]}")
                self.serversCode[str(indx)] = server[0]
                indx += 1

        except Exception as e:
            mrpac_logger.error(e)
            mrpac_logger.debug(e, exc_info=True)

    def addFunc(self):
        """Start the Add window."""
        self.add_ = AddWindow()

    def removeFunc(self):
        """Remove a selected DICOM location from database."""
        if self.selectedItem:
            try:
                idx_ = self.selectedItem[0]
                id_ = self.serversCode[idx_]
                query = "DELETE FROM servers WHERE id = ?"
                try:
                    Globals.db_cur.execute(query, (id_,))
                    Globals.db_con.commit()
                    self.startUp()
                except Exception as e:
                    print(e)
            except Exception as e:
                database_logger.error(e)
                database_logger.debug(e, exc_info=True)

    def itemClicked_event(self, item):
        """Get the info of the DICOM location that is clicked on.

        Parameters
        ----------
        item : `QListWidgetItem`
            The selected DICOM location.
        """
        self.selectedItem = item.text()
        self.selectedItem = self.selectedItem.split(" ")

    def itemActivated_event(self, item):
        """Activate the DICOM location selected.

        Activates the DICOM location that is selected in this
        `ConfigScreen` and shows the details in the `Server Details`
        section of the `SetupScreen`.

        Parameters
        ----------
        item : `QListWidgetItem`
            The DICOM location to activate.
        """

        self.selectedItem = item.text()
        self.selectedItem = self.selectedItem.split(" ")

        try:
            idx_ = self.selectedItem[0]
            id_ = self.serversCode[idx_]
            query = "SELECT * FROM servers WHERE id = ?"
            Globals.selected_server = Globals.db_cur.execute(query, (id_,)).fetchone()
            Globals.setup_screen.serverSelected()
            self.close()
        except Exception as e:
            mrpac_logger.error(e)
            mrpac_logger.debug(e, exc_info=True)

    def editFunc(self):
        """Edit the details of the selected DICOM location."""

        if self.selectedItem:
            try:
                idx_ = self.selectedItem[0]
                id_ = self.serversCode[idx_]
                query = "SELECT * FROM servers WHERE id = ?"
                Globals.selected_server = Globals.db_cur.execute(query, (id_,)).fetchone()
                self.edit = EditWindow()
            except Exception as e:
                mrpac_logger.error(e)
                mrpac_logger.debug(e, exc_info=True)

    def selectFunc(self):
        """Activate the selected DICOM location."""

        try:
            idx_ = self.selectedItem[0]
            id_ = self.serversCode[idx_]
            query = "SELECT * FROM servers WHERE id = ?"
            Globals.selected_server = Globals.db_cur.execute(query, (id_,)).fetchone()
            self.close()
            Globals.setup_screen.serverSelected()
        except Exception as e:
            mrpac_logger.error(e)
            mrpac_logger.debug(e, exc_info=True)
