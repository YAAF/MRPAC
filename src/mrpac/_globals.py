"""Global variables for MRPAC"""

import logging
import os
import socket
import inspect
import sqlite3
from datetime import datetime
from typing import Union


class Globals:
    # Directory paths
    PARENT_DIRECTORY = os.path.join(os.path.dirname(__file__))
    RESOURCES_DIRECTORY = os.path.join(PARENT_DIRECTORY, "resources")
    MODELS_DIRECTORY = os.path.join(PARENT_DIRECTORY, "models")
    LOGS_DIRECTORY = os.path.join(PARENT_DIRECTORY, "logs")
    TEMP_DIRECTORY = os.path.join(PARENT_DIRECTORY, "Temp")

    # Set the log formatter
    LOG_FORMATTER = logging.Formatter("%(asctime)s:%(levelname)s:%(message)s:%(lineno)d")

    # Get the UID prefix if present
    try:
        with open(os.path.join(RESOURCES_DIRECTORY, "uid_prefix.txt"), "r") as uid:
            UID_PREFIX: Union[str, None] = uid.readline()
    except FileNotFoundError:
        UID_PREFIX = None

    # Get the IP address of this device
    HOSTIP = socket.gethostbyname(socket.gethostname())
    scpAET = None
    scpPort = None

    # Set global variables
    current_user = None
    user_type = None
    selected_server = None
    setup_screen = None
    config_screen = None
    current_dicom = None

    db_cur = None
    db_con = None

    log_db_path = os.path.join(LOGS_DIRECTORY, "log.db")
    log_db_cursor = None
    log_db_conn = None

    @staticmethod
    def log_to_db(logger_name, log_level, message, stack_trace=None):
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        # Automatically retrieve the caller's current line number
        frame = inspect.currentframe()
        caller_frame = frame.f_back  # Go back one step in the call stack
        line_number = caller_frame.f_lineno

        if logger_name == "mrpac":
            insert_sql = """INSERT INTO mrpac_log (Timestamp, LogLevel, Message, LineNumber, StackTrace)
                        VALUES (?, ?, ?, ?, ?)"""
        elif logger_name == "autocontour":
            insert_sql = """INSERT INTO autocontour_log (Timestamp, LogLevel, Message, LineNumber, StackTrace)
                        VALUES (?, ?, ?, ?, ?)"""
        elif logger_name == "database":
            insert_sql = """INSERT INTO database_log (Timestamp, LogLevel, Message, LineNumber, StackTrace)
                        VALUES (?, ?, ?, ?, ?)"""
        elif logger_name == "network":
            insert_sql = """INSERT INTO network_log (Timestamp, LogLevel, Message, LineNumber, StackTrace)
                        VALUES (?, ?, ?, ?, ?)"""
        else:
            insert_sql = """INSERT INTO mrpac_log (Timestamp, LogLevel, Message, LineNumber, StackTrace)
                        VALUES (?, ?, ?, ?, ?)"""

        Globals.log_db_conn = sqlite3.connect(Globals.log_db_path)
        Globals.log_db_cursor = Globals.log_db_conn.cursor()

        Globals.log_db_cursor.execute(
            insert_sql, (timestamp, log_level, message, line_number, stack_trace)
        )

        Globals.log_db_conn.commit()
        Globals.log_db_conn.close()
