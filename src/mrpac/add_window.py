import logging

from PySide6 import QtCore as qtc
from PySide6 import QtWidgets as qtw
from PySide6 import QtGui as qtg

from ._globals import Globals
from .DICOM_Networking import (
    pingTest,
    validEntry,
    verifyEcho,
)
from .ui_add import Ui_AddWindow
from .verify_window import VerifyWindow

mrpac_logger = logging.getLogger("mrpac")
database_logger = logging.getLogger("database")
pynet_logger = logging.getLogger("network")


class AddWindow(qtw.QWidget, Ui_AddWindow):
    """The window to add new DICOM location."""

    def __init__(self):
        """Initialize the add window."""

        super().__init__()
        self.setupUi(self)
        self.setFixedHeight(300)
        self.setFixedWidth(400)
        self.addButton.clicked.connect(self.addFunc)
        self.cancelButton.clicked.connect(self.cancelFunc)
        self.verifyConButton.clicked.connect(self.verifyConFunc)

        self.show()

    def addFunc(self):
        """Add the new DICOM location when add button is clicked."""

        serverAET = self.serverAETEntry.text()
        serverIP = self.serverIPEntry.text()
        serverPort = self.serverPortEntry.text()
        if serverAET == "" or serverIP == "" or serverPort == "":
            self.invalidLabel.setText("Fields cannot be empty.")
            mrpac_logger.error("ADD--Server Details: Fields cannot be emtpy.")
        elif not validEntry(serverAET, "AET"):
            self.invalidLabel.setText("Invalid AET")
            mrpac_logger.error("ADD--Server Details: Invalid AE Title")
        elif not validEntry(serverIP, "IP"):
            self.invalidLabel.setText("Invalid IP")
            mrpac_logger.error("ADD--Server Details: Invalid IP address.")
        elif not validEntry(serverPort, "Port"):
            self.invalidLabel.setText("Invalid port")
            mrpac_logger.error("ADD--Server Details: Invalid port number.")

        else:
            query = "INSERT INTO servers (serverAET, serverIP, serverPort) VALUES (?, ?, ?)"
            try:
                Globals.db_cur.execute(query, (serverAET, serverIP, serverPort))
                Globals.db_con.commit()
                self.close()

            except Exception as e:
                self.invalidLabel.setText("Could not add server")
                database_logger.error(e)
                database_logger.debug(e, exc_info=True)

    def cancelFunc(self):
        """Close the Add window."""

        self.close()

    def verifyConFunc(self):
        """Send a C-Echo to verify DICOM connectivity."""

        serverAET = self.serverAETEntry.text()
        serverIP = self.serverIPEntry.text()
        serverPort = self.serverPortEntry.text()
        pingResult = None
        echoResult = None
        if serverAET == "" or serverIP == "" or serverPort == "":
            self.invalidLabel.setText("Fields cannot be empty.")
            mrpac_logger.error("ADD--Server Details: Fields cannot be empty.")
        elif not validEntry(serverAET, "AET"):
            self.invalidLabel.setText("Invalid AET")
            mrpac_logger.error("ADD--Server Details: Invalid AE Tile.")
        elif not validEntry(serverIP, "IP"):
            self.invalidLabel.setText("Invalid IP")
            mrpac_logger.error("ADD--Server Details: Invalid IP address.")
        elif not validEntry(serverPort, "Port"):
            self.invalidLabel.setText("Invalid port")
            mrpac_logger.error("ADD--Server Details: Invalid port number.")
        else:
            try:
                pingResult = pingTest(serverIP)
            except Exception as e:
                pingResult = "Failed"
                mrpac_logger.error(e)

            try:
                echoResult = verifyEcho(Globals.scpAET, serverAET, serverIP, int(serverPort))
            except Exception as e:
                echoResult = "Failed"
                mrpac_logger.error(e)

        self.verify = VerifyWindow(pingResult, echoResult)
