import logging


from PySide6 import QtCore as qtc
from PySide6 import QtWidgets as qtw
from PySide6 import QtGui as qtg

from ._globals import Globals
from .DICOM_Networking import (
    pingTest,
    validEntry,
    verifyEcho,
)
from .ui_edit import Ui_Edit
from .verify_window import VerifyWindow

mrpac_logger = logging.getLogger("mrpac")
database_logger = logging.getLogger("database")
pynet_logger = logging.getLogger("network")


class EditWindow(qtw.QWidget, Ui_Edit):
    """The Edit window to modify DICOM location info."""

    def __init__(self):
        """Initialize the Edit window."""

        super().__init__()
        self.setupUi(self)
        self.setFixedHeight(300)
        self.setFixedWidth(400)
        self.updateButton.clicked.connect(self.updateFunc)
        self.cancelButton.clicked.connect(self.cancelFunc)
        self.verifyConButton.clicked.connect(self.verifyConFunc)

        self.startUp()

        self.show()

    def startUp(self):
        """Start the Edit window and show details of the DICOM location to be edited."""

        self.serverAETEntry.setText(Globals.selected_server[1])
        self.serverIPEntry.setText(Globals.selected_server[2])
        self.serverPortEntry.setText(Globals.selected_server[3])

    def updateFunc(self):
        """Update the DICOM location details with the new info."""

        serverAET = self.serverAETEntry.text()
        serverIP = self.serverIPEntry.text()
        serverPort = self.serverPortEntry.text()
        if serverAET == "" or serverIP == "" or serverPort == "":
            self.invalidLabel.setText("Fields cannot be empty.")
            mrpac_logger.error("EDIT--Server Details: Fields cannot be empty.")
        elif not validEntry(serverAET, "AET"):
            self.invalidLabel.setText("Invalid AET")
            mrpac_logger.error("EDIT--Server Details: Invalit AE Title.")
        elif not validEntry(serverIP, "IP"):
            self.invalidLabel.setText("Invalid IP")
            mrpac_logger.error("EDIT--Server Details: Invalid IP address.")
        elif not validEntry(serverPort, "Port"):
            self.invalidLabel.setText("Invalid Port")
            mrpac_logger.error("EDIT--Server Details: Invalid port number.")
        else:
            query = "UPDATE servers set serverAET = ?, serverIP = ?, serverPort = ? WHERE id = ?"
            try:
                Globals.db_cur.execute(
                    query, (serverAET, serverIP, serverPort, Globals.selected_server[0])
                )
                Globals.db_con.commit()
                Globals.config_screen.startUp()
                self.close()
            except Exception as e:
                self.invalidLabel.setText("Could not update server")
                database_logger.error(e)
                database_logger.debug(e, exc_info=True)

    def cancelFunc(self):
        """Close the Edit window."""

        self.close()

    def verifyConFunc(self):
        """Verify the DICOM connection using ping and C-Echo."""

        serverAET = self.serverAETEntry.text()
        serverIP = self.serverIPEntry.text()
        serverPort = self.serverPortEntry.text()
        pingResult = None
        echoResult = None
        if serverAET == "" or serverIP == "" or serverPort == "":
            self.invalidLabel.setText("Fields cannot be empty.")
            mrpac_logger.error("VERIFY: Fields cannot be empty.")
        elif not validEntry(serverAET, "AET"):
            self.invalidLabel.setText("Invalid AET")
            mrpac_logger.error("VERIFY: Invalid AE Title.")
        elif not validEntry(serverIP, "IP"):
            self.invalidLabel.setText("Invalid IP")
            mrpac_logger.error("VERIFY: Invalid IP address.")
        elif not validEntry(serverPort, "Port"):
            self.invalidLabel.setText("Invalid port")
            mrpac_logger.error("VERIFY: Invalid port number.")
        else:
            try:
                pingResult = pingTest(serverIP)
            except Exception as e:
                pingResult = "Failed"
                mrpac_logger.error(e)

            try:
                echoResult = verifyEcho(Globals.scpAET, serverAET, serverIP, int(serverPort))
            except Exception as e:
                echoResult = "Failed"
                mrpac_logger.error(e)

        self.verify = VerifyWindow(pingResult, echoResult)
