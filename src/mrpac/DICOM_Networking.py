"""A module for DICOM networking using pynetdicom."""

import datetime
import logging
import os
from typing import Union
import shutil
import subprocess
import sys
import traceback
from ping3 import ping
from pydicom import dcmread
from pynetdicom import AE, StoragePresentationContexts, evt
from pynetdicom.events import EventHandlerType
from pynetdicom.sop_class import Verification

from ._globals import Globals

# Initialize the Logger files
pynet_logger = logging.getLogger("network")
mrpac_logger = logging.getLogger("mrpac")
database_logger = logging.getLogger("database")


evt.EVT_PDU_RECV


def validEntry(input_text: Union[str, int], entry_type: str) -> bool:
    """Checks whether a text input from the user contains invalid
    characters.

    Parameters
    ----------
    input_text : Union[str, int]
        The text input to a given field.
    entry_type : str
        The type of field where the text was input. The different
        types are:
        * AET
        * Port
        * IP

    Returns
    -------
    bool
        Whether the input was valid or not.
    """
    if (
        " " in input_text
        or '"' in input_text
        or "'" in input_text
        or "\\" in input_text
        or "*" in input_text
    ):
        return False
    else:
        if entry_type == "AET":
            return True
        elif entry_type == "Port":
            try:
                int(input_text)
                return True
            except ValueError:
                return False
        elif entry_type == "IP":
            return True
        else:
            return False


def pingTest(ip: str) -> str:
    """Verify whether the device with the ip address typed accepts
    packets over the network.

    Parameters
    ----------
    ip : str
        The IPv4 address of the device to ping.

    Returns
    -------
    str
        Success or Failed.
    """

    response = ping(ip, timeout=0.01)

    if response is None:
        return "Failed"
    elif response is False:
        return "Failed"
    else:
        return "Success"


def verifyEcho(scpAET: str, aet: str, ip: str, port: Union[str, int]) -> str:
    """Verifies whether a DICOM association can be established given
    an AE (Application Entity) title, IP address and port number of a
    peer AE.

    Parameters
    ----------
    scpAET : str
        A calling AE title to use.
    aet : str
        The AE title of the peer AE.
    ip : str
        The IPv4 address of the peer AE.
    port : Union[str, int]
        The port number of the peer AE.

    Returns
    -------
    str
        A DICOM response status as a string or `Failed` if association
        could not be established or no response was received.
    """

    ae = AE(scpAET)
    ae.add_requested_context(Verification)
    assoc = ae.associate(ip, int(port), ae_title=aet)
    result = None
    if assoc.is_established:
        status = assoc.send_c_echo()
        if status:
            result = "{0:04x}".format(status.Status)
        else:
            result = "Failed"
    else:
        result = "Failed"

    assoc.release()
    return result


def send_c_store(recAET: str, recIP: str, recPort: Union[str, int], struct_path: str) -> None:
    """Start a StorageSCU AE and send the given DICOM file.

    Parameters
    ----------
    recAET : str
        The AE title for the called AE.
    recIP : str
        The IP address for the called AE.
    recPort : Union[str, int]
        The port number for the called AE.
    struct_path : str
        The path to the DICOM file to be sent.
    """
    try:
        storagescu = StorageSCU(recAET, recIP, recPort)
    except Exception as e:
        pynet_logger.error(e)
        pynet_logger.debug(e, exc_info=True)
        stack_trace = traceback.format_exc()
        Globals.log_to_db("network", "ERROR", str(e), None)
        Globals.log_to_db("network", "DEBUG", str(e), stack_trace)

    try:
        storagescu.c_store(os.path.join(struct_path, "AutoContour.dcm"))
    except Exception as e:
        pynet_logger.error(e)
        pynet_logger.debug(e, exc_info=True)
        stack_trace = traceback.format_exc()
        Globals.log_to_db("network", "ERROR", str(e), None)
        Globals.log_to_db("network", "DEBUG", str(e), stack_trace)


def handle_open(event):
    """Log the remote's (host, port) when connected."""

    msg = "Connected with remote at {}".format(event.address)
    pynet_logger.info(msg)
    Globals.log_to_db("network", "INFO", msg, None)


# Implement the handler for evt.EVT_C_STORE
def handle_store(event):
    """Handle a C-STORE request event."""

    Globals.current_dicom = {}

    ds = event.dataset
    ds.file_meta = event.file_meta

    Globals.current_dicom["patientName"] = ds.PatientName
    Globals.current_dicom["patientID"] = ds.PatientID
    Globals.current_dicom["modality"] = ds.Modality
    Globals.current_dicom["time"] = str(datetime.datetime.now())

    try:
        os.makedirs(Globals.TEMP_DIRECTORY)
    except OSError:
        pass

    path = os.path.join(Globals.TEMP_DIRECTORY, ds.PatientID)
    StorageSCP.slices_path = os.path.join(path, ds.Modality)
    try:
        os.makedirs(StorageSCP.slices_path)
    except OSError:
        pass

    # Save the dataset using the SOP Instance UID as the filename
    outfile = os.path.join(StorageSCP.slices_path, ds.SOPInstanceUID + ".dcm")
    ds.save_as(outfile, write_like_original=False)

    try:
        Globals.setup_screen.contouringStatus.setText(
            f"Receiving {str(ds.Modality)} for {str(ds.PatientName)} {str(ds.PatientID)}"
        )

    except Exception as e:
        pynet_logger.error(e)
        pynet_logger.debug(e, exc_info=True)
        stack_trace = traceback.format_exc()
        Globals.log_to_db("network", "ERROR", str(e), None)
        Globals.log_to_db("network", "DEBUG", str(e), stack_trace)

    # Return a 'Success' status
    return 0x0000


def handle_close(event):
    """
    Handle processes when closing the Move AE.

    When the association is closed after receiving the DICOM image,
    it runs the Autocontour program to generate segmentations for the DICOM image.
    The Autocontour program saves the segmentations as RTstruct. The RTstruct
    is then sent to the DICOM location selected at the Setup window.
    """
    msg = "Disconnected from remote at {}".format(event.address)
    pynet_logger.info(msg)
    Globals.log_to_db("network", "INFO", msg, None)

    try:
        Globals.setup_screen.contouringStatus.setText("")
    except Exception as e:
        mrpac_logger.error(e)
        mrpac_logger.debug(e, exc_info=True)
        stack_trace = traceback.format_exc()
        Globals.log_to_db("mrpac", "ERROR", str(e), None)
        Globals.log_to_db("mrpac", "DEBUG", str(e), stack_trace)

    if StorageSCP.slices_path != "":
        slices_path = os.path.abspath(StorageSCP.slices_path)
        StorageSCP.slices_path = ""
        parPath = os.path.abspath(os.path.join(slices_path, os.pardir))
        struct_path = os.path.join(parPath, "RTstruct")
        status = "Success"
        error = ""
        try:
            if Globals.selected_server:
                serverAET = Globals.selected_server[1]
                serverIP = Globals.selected_server[2]
                serverPort = Globals.selected_server[3]
                serverPort = int(serverPort)

                try:
                    Globals.setup_screen.contouringStatus.setText(
                        f"Autocontouring {str(Globals.current_dicom['modality'])} "
                        + f"for {str(Globals.current_dicom['patientName'])}"
                    )
                    if Globals.current_dicom["modality"] == "MR":
                        try:
                            AutocontourMR_path = os.path.join(
                                Globals.PARENT_DIRECTORY, "AutocontourMR.py"
                            )
                            subprocess.run(
                                f"{sys.executable} {AutocontourMR_path} "
                                f"{slices_path} {struct_path} {Globals.UID_PREFIX}",
                            )
                        except Exception as e:
                            status = "Failed"
                            error = str(e)
                            mrpac_logger.error(e)
                            mrpac_logger.debug(e, exc_info=True)
                            stack_trace = traceback.format_exc()
                            Globals.log_to_db("mrpac", "ERROR", str(e), None)
                            Globals.log_to_db(
                                "mrpac",
                                "DEBUG",
                                str(e),
                                stack_trace,
                            )
                    elif Globals.current_dicom["modality"] == "CT":
                        try:
                            import totalsegmentator

                            AutocontourCT_path = os.path.join(
                                Globals.PARENT_DIRECTORY, "AutocontourCT.py"
                            )
                            subprocess.run(
                                f"{sys.executable} {AutocontourCT_path} "
                                f"{slices_path} {struct_path} {Globals.UID_PREFIX}",
                            )
                        except ModuleNotFoundError as mnf:
                            status = "Failed"
                            error = str(mnf)
                            Globals.setup_screen.contouringStatus.setText(
                                "TotalSegmentator is not installed."
                            )
                            mrpac_logger.error(mnf)
                            Globals.log_to_db("mrpac", "ERROR", str(e), None)

                        except Exception as e:
                            status = "Failed"
                            error = str(e)
                            mrpac_logger.error(e)
                            mrpac_logger.debug(e, exc_info=True)
                            stack_trace = traceback.format_exc()
                            Globals.log_to_db("mrpac", "ERROR", str(e), None)
                            Globals.log_to_db(
                                "mrpac",
                                "DEBUG",
                                str(e),
                                stack_trace,
                            )

                    try:
                        Globals.setup_screen.contouringStatus.setText(
                            f"Transferring RTSTRUCT to {serverAET}"
                        )
                        send_c_store(serverAET, serverIP, serverPort, struct_path)
                    except Exception as e:
                        status = "Failed"
                        error = str(e)
                        msg = (
                            f"Failed to transfer RTSTRUCT to {serverAET} for "
                            + str(Globals.current_dicom["patientName"])
                            + " "
                            + str(Globals.current_dicom["patientID"])
                            + ". "
                            + str(e)
                        )
                        pynet_logger.error(msg)
                        pynet_logger.debug(e, exc_info=True)

                        stack_trace = traceback.format_exc()
                        Globals.log_to_db("network", "ERROR", str(e), None)
                        Globals.log_to_db(
                            "network",
                            "DEBUG",
                            str(e),
                            stack_trace,
                        )

                except Exception as e:
                    status = "Failed"
                    error = str(e)
                    msg = (
                        "Failed to autocontour for "
                        + str(Globals.current_dicom["patientName"])
                        + " "
                        + str(Globals.current_dicom["patientID"])
                        + ". "
                        + str(e)
                    )
                    mrpac_logger.error(msg)
                    mrpac_logger.debug(e, exc_info=True)

                    stack_trace = traceback.format_exc()
                    Globals.log_to_db("mrpac", "ERROR", str(e), None)
                    Globals.log_to_db(
                        "mrpac",
                        "DEBUG",
                        str(e),
                        stack_trace,
                    )
                shutil.rmtree(slices_path)
                shutil.rmtree(struct_path)
            else:
                shutil.rmtree(slices_path)
                status = "Failed"
                error = "No server selected."
                msg = (
                    f"Failed to autocontour for "
                    + str(Globals.current_dicom["patientName"])
                    + " "
                    + str(Globals.current_dicom["patientID"])
                    + f". {error}"
                )
                pynet_logger.error(msg)

                Globals.log_to_db("network", "ERROR", msg, None)
            msg = (
                f"Successfully transferred RTStruct to {serverAET} for "
                + str(Globals.current_dicom["patientName"])
                + " "
                + str(Globals.current_dicom["patientID"])
            )
            pynet_logger.info(msg)

            Globals.log_to_db("network", "INFO", msg, None)
        except Exception as e:
            status = "Failed"
            error = str(e)
            msg = (
                f"Failed to autocontour for "
                + str(Globals.current_dicom["patientName"])
                + " "
                + str(Globals.current_dicom["patientID"])
                + ". "
                + str(e)
            )
            pynet_logger.error(msg)
            pynet_logger.debug(e, exc_info=True)

            stack_trace = traceback.format_exc()
            Globals.log_to_db("network", "ERROR", msg, None)
            Globals.log_to_db(
                "network",
                "DEBUG",
                str(e),
                stack_trace,
            )

        try:
            query = "INSERT INTO history \
                (patientName, patientID, time, status, errors) \
                    VALUES (?, ?, ?, ?, ?)"
            Globals.db_cur.execute(
                query,
                (
                    str(Globals.current_dicom["patientName"]),
                    str(Globals.current_dicom["patientID"]),
                    str(Globals.current_dicom["time"]),
                    status,
                    error,
                ),
            )
            Globals.db_con.commit()
        except Exception as e:
            msg = (
                "Failed to record status for "
                + str(Globals.current_dicom["patientName"])
                + str(Globals.current_dicom["patientID"])
                + ". "
                + str(e)
            )
            database_logger.error(msg)
            database_logger.debug(e, exc_info=True)

            stack_trace = traceback.format_exc()
            Globals.log_to_db("database", "ERROR", msg, None)
            Globals.log_to_db(
                "database",
                "DEBUG",
                str(e),
                stack_trace,
            )

        Globals.setup_screen.contouringStatus.setText("")


class StorageSCU:
    """A DICOM SCU for C-Store requests."""

    def __init__(self, recAET: str, recIP: str, recPort: Union[str, int]) -> None:
        """Initialize the SCU with the given parameters.

        Parameters
        ----------
        recAET : str
            The called AE title.
        recIP : str
            The called AE IPv4 address.
        recPort : Union[str, int]
            The called AE port number.
        """
        self.recAET = recAET
        self.recIP = recIP
        self.recPort = int(recPort)

        self.ae = AE("MRPAC")
        self.ae.requested_contexts = StoragePresentationContexts

    def c_store(self, dcmFile_path: str) -> None:
        """Send C-Store request with the given DICOM file.

        Parameters
        ----------
        dcmFile_path : str
            The path to the DICOM file to be sent.
        """
        ds_file = dcmread(dcmFile_path)
        self.assoc = self.ae.associate(self.recIP, self.recPort, ae_title=self.recAET)
        if self.assoc.is_established:
            status = self.assoc.send_c_store(ds_file)
            if status:
                # If the storage request succeeded this will be 0x0000
                msg = "C-STORE request status: 0x{0:04x}".format(status.Status)
                pynet_logger.info(msg)
                Globals.log_to_db("network", "INFO", msg, None)
            else:
                msg = "Connection timed out, was aborted or received invalid response"
                pynet_logger.error(msg)
                Globals.log_to_db("network", "ERROR", msg, None)
            self.assoc.release()
        else:
            msg = "Association rejected, aborted or never connected"
            pynet_logger.error(msg)
            Globals.log_to_db("network", "ERROR", msg, None)


class StorageSCP:
    """A DICOM SCP that handles store requests."""

    slices_path = ""

    def __init__(self, aet: str, ip: str, port: Union[str, int]) -> None:
        """Initialize the SCP to handle store requests.

        Parameters
        ----------
        aet : str
            The AE title to use.
        ip : str
            The IPv4 address to use.
        port : Union[str, int]
            The port number to use (make sure it is not already used
            by another application on your computer).
        """
        self.scpAET = aet
        self.scpIP = ip
        self.scpPort = int(port)

        self.ae = AE(self.scpAET)

        # Add the supported presentation context (All Storage Contexts)
        self.ae.supported_contexts = StoragePresentationContexts
        self.ae.add_supported_context(Verification)

    def set_handlers(
        self,
        handle_open: EventHandlerType = None,
        handle_close: EventHandlerType = None,
        handle_store: EventHandlerType = None,
    ) -> None:
        """Set event handlers for this SCP.

        Parameters
        ----------
        handle_open : EventHandlerType, optional
            A handler function that is executed during connection
            establishment, by default None.
        handle_close : EventHandlerType, optional
            A handler function that is executed when association is
            released, by default None.
        handle_store : EventHandlerType, optional
            A handler function that is executed when C-Store requests
            are received, by default None.
        """
        self.handlers = []
        if handle_open:
            self.handle_open = handle_open
            self.handlers.append((evt.EVT_CONN_OPEN, self.handle_open))
        if handle_close:
            self.handle_close = handle_close
            self.handlers.append((evt.EVT_CONN_CLOSE, self.handle_close))
        if handle_store:
            self.handle_store = handle_store
            self.handlers.append((evt.EVT_C_STORE, self.handle_store))

    def start(self):
        """Start the DICOM SCP server."""
        Globals.log_to_db("network", "INFO", "Starting the DICOM SCP server ...")
        self.scp = self.ae.start_server(
            (self.scpIP, self.scpPort), block=False, evt_handlers=self.handlers
        )

    def stop(self):
        """Stop the DICOM SCP server."""
        Globals.log_to_db("network", "INFO", "Stopping the DICOM SCP server...")
        self.scp.unbind(evt.EVT_CONN_OPEN, self.handle_open)
        self.scp.bind(evt.EVT_CONN_CLOSE, self.handle_close)
        self.scp.shutdown()
