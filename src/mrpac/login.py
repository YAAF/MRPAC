import logging

import bcrypt

from PySide6 import QtCore as qtc
from PySide6 import QtWidgets as qtw
from PySide6 import QtGui as qtg

from ._globals import Globals
from .DICOM_Networking import validEntry
from .ui_login import Ui_Login
from .setup_window import SetupWindow

mrpac_logger = logging.getLogger("mrpac")
database_logger = logging.getLogger("database")


class LoginWindow(qtw.QWidget, Ui_Login):
    """The login window configuration."""

    def __init__(self):
        """Initialize the login window."""
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle("Login")
        self.setFixedHeight(220)
        self.setFixedWidth(400)
        self.loginButton.clicked.connect(self.loginFunc)

        self.show()

    def loginFunc(self):
        """Log in the user when the button is clicked."""

        if self.usernameEntry.text() == "" or self.passwordEntry.text() == "":
            pass
        else:
            username_entered = self.usernameEntry.text()
            passphrase_entered = self.passwordEntry.text()
            if not validEntry(username_entered, "AET") or not validEntry(
                passphrase_entered, "AET"
            ):
                self.invalidLabel.setText("Invalid username or password")
                mrpac_logger.error("LOGIN: Invalid username or password.")
            else:
                query = "SELECT * FROM users WHERE userName = ?"
                try:
                    Globals.db_cur.execute(query, (username_entered,))
                    usr_info = Globals.db_cur.fetchone()
                    stored_passPhrase = usr_info[2]

                    if bcrypt.checkpw(
                        passphrase_entered.encode("utf-8"),
                        stored_passPhrase.encode("utf-8"),
                    ):
                        Globals.user_type = usr_info[3]
                        Globals.setup_screen = SetupWindow()
                        self.close()

                    else:
                        self.invalidLabel.setText("Invalid username or password")
                        database_logger.error("LOGIN: Invalid username or password.")
                except Exception as e:
                    self.invalidLabel.setText("Invalid username or password")
                    database_logger.error(e)
                    database_logger.debug(e, exc_info=True)
